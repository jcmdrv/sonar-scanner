# Sonar scanner
This container allows you to perform static analysis using sonar-scanner and to publish the results to a SonarQube instance; sonarcloud.io by default.

# Run a Sonar analysis
In order to analyze your JS project using sonar-scanner.

- Review your *sonar-scanner.properties" file
```
#----- Default SonarQube server
sonar.host.url=https://sonarcloud.io

#----- Default source code encoding
#sonar.sourceEncoding=UTF-8

sonar.organization=davidrova-bitbucket
sonar.projectKey=techubank-auth_server
sonar.projectVersion=1.0.0
sonar.projectName=techubank-auth-server
sonar.sources=src
sonar.javascript.lcov.reportPaths=report-coverage/lcov.info
sonar.exclusions=**/node_modules/**,**/test/**,**/reports/**,**/bower_components/**,*.min.js,**/demo/**,**/coverage-reports/**,**/dist/**,components/*analytics*/**
```

- Run the command:
```
docker run -it --mount type=bind,source="$(pwd)",target=/root/project -v "$(pwd)"/sonar-scanner.properties,target=/root/sonar-scanner/conf/sonar-scanner.properties --env SONAR_API_KEY=<SONAR_USER_API_KEY> davidrova/sonar-scanner:latest 
```


# Build the Docker image
```
docker build -t davidrova/sonar-scanner .
```

# Publish sonar-scanner in Docker Hub
```
// you must be first login into docker hub using
docker login

// push the build
docker push davidrova/sonar-scanner
```

